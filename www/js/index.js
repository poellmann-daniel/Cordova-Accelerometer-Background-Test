$(document).on("deviceready", function() {

    // Activate Background mode (doesn't help a bit for the problem)
    cordova.plugins.backgroundMode.enable(); // uncomment and background mode is disabled

    // Setup Audio Playback
    plugin_Sound.file("shot.mp3");

    // Setup Accelerometer Interval
    function onSuccess(acceleration) {
        console.log(acceleration.timestamp);
        plugin_Sound.play();
    }

    function onError() {
        alert('onError!');
    }

    var options = { frequency: 300 };  // Update every 300 ms

    var watchID = navigator.accelerometer.watchAcceleration(onSuccess, onError, options);

});