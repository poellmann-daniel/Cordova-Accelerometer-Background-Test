var plugin_Sound = {

    media: null,

    file: function(filelocation) {

        filelocation = "file:///android_asset/www/" + filelocation;

        if (this.media != null) {
            this.media.release();
        }

        this.media = new Media(filelocation);
    },

    play: function(location) {
        this.media.play();
    },

    getCurrentPosition: function(callback) {
        this.media.getCurrentPosition(callback);
    },

    getDuration: function() {
        return this.media.getDuration();
    },

    pause: function() {
        this.media.pause();
    },

    seekTo: function(milliseconds) {
        this.media.seekTo(milliseconds);
    },

    setVolume: function(volume) {
        this.media.setVolume(volume);
    },

    stop: function() {
        if (this.media != null) {
            this.media.stop();
        }
    }

};

